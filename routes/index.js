var express = require('express');
var router = express.Router();

var natural = require('natural');
var tokenizer = new natural.WordTokenizer();

/* GET home page. */
router.get('/', function(req, res, next) {
  let words = ['accept', 'arrange', 'balance', 'encourage', 'huge', 'purpose', 'release'];

  res.io.on('connection', function(socket) {
    console.log('Client connected...');
    socket.on('speaker_input', function(data) {
      socket.emit('compare_return', natural.JaroWinklerDistance(data.origin_word, data.speaker_word));
    });
  });

  res.render('index', {
    title: 'Speak checker',
    words: words
  });
});

module.exports = router;
